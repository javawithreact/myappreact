import React, {Component} from 'react';
import AuthenticationService from './AuthenticationService.js'

class LoginComponent extends Component {

    constructor(props) {
        super(props)
        this.state = {
            username: 'user',
            password: 'password',
            hasLoginFailed: false,
            showSuccessMessage: false
        }

        this.handleChange = this.handleChange.bind(this);
        this.loginClicked = this.loginClicked.bind(this);
    }

    handleChange(event) {
        this.setState(
            {
                [event.target.name]: event.target.value
            })
    }

    loginClicked() {

        // if (this.state.username === 'user' && this.state.password === 'password') {
        //     AuthenticationService.registerSuccessfulLogin(this.state.username, this.state.password);
        //     this.props.history.push(`/welcome/${this.state.username}`)
        //     this.setState({showSuccessMessage: true})
        //     this.setState({hasLoginFailed: false})
        // } else {
        //     this.props.history.push("/login")
        //     this.setState({showSuccessMessage: false})
        //     this.setState({hasLoginFailed: true})
        // }
        // AuthenticationService.executeBasicAuthenticationService(this.state.username, this.state.password)
        // .then( () => {
        //             AuthenticationService.registerSuccessfulLogin(this.state.username, this.state.password);
        //             this.props.history.push(`/welcome/${this.state.username}`)
        //         }).catch( () => {
        //             this.setState({showSuccessMessage: false})
        //             this.setState({hasLoginFailed: true})
        //         })
        
        AuthenticationService.executeJwtAuthenticationService(this.state.username, this.state.password)
        .then( (response) => {
                    AuthenticationService.registerSuccessfulLoginForJwt(this.state.username, response.data.token);
                    this.props.history.push(`/welcome/${this.state.username}`)
                }).catch( () => {
                    this.setState({showSuccessMessage: false})
                    this.setState({hasLoginFailed: true})
                })
    }

    render() {
        return (
            <div>
                <h1>Login</h1>
                <div className="container">
                    {this.state.hasLoginFailed && <div className="alert alert-warning">Invalid credentials</div>}
                    {this.state.showSuccessMessage && <div>Login successful</div>}
                    User name: <input type="text" name="username" value={this.state.username}
                                      onChange={this.handleChange}></input>
                    Password: <input type="password" name="password" value={this.state.password}
                                     onChange={this.handleChange}></input>
                    <button onClick={this.loginClicked} className="btn btn-success">Login</button>
                </div>
            </div>
        )
    }
}

export default LoginComponent