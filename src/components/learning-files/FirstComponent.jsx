import React, {Component} from 'react'

//Class component
class FirstComponent extends Component {
    render() {
        return (
            <div className="firstComponent">
                <h1>First Component</h1>
            </div>
        )
    }
}

export default FirstComponent